package actionscore

import "fmt"
import "github.com/golang/glog"
import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/local"
import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-actions/runner/group"

// Split - actions.split
func Split(ctx action.Map) (action.Map, error) {
	glog.V(2).Info("actions.split start\n")
	split, ok := ctx.Pop("split").Map()
	if !ok {
		return nil, fmt.Errorf("Split value %#v is not valid", split)
	}

	// Create a running group
	g := group.New()

	// Split execution
	for key := range split {
		// What to do
		do, _ := split.Pop(key).Map()
		// Action name
		name, _ := do.Get("name").String()
		actx, _ := do.Get("ctx").Map()
		if actx == nil {
			actx = make(action.Map)
		}

		// Name of pushed iterated value
		as, _ := do.Get("as").String()

		// Value to split
		value := ctx.Pop(key)

		// If its a value -- iterate on it and run action on every element
		if list, ok := value.List(); ok {
			for _, v := range list {
				// run `do` action with `v` as `do.as`
				c := actx.Clone()
				c[as] = v
				g.Add(&action.Action{Name: name, Ctx: c})
				glog.V(2).Infof("actions.split run name=%s ctx=%#v\n", name, c)
			}
		} else {
			// run `do` action with `value` as `do.as`
			actx[as] = value.Value
			g.Add(&action.Action{Name: name, Ctx: actx})
			glog.V(2).Infof("actions.split run name=%s ctx=%#v\n", name, actx)
		}
	}

	err := <-g.Wait()
	if err != nil {
		glog.V(2).Infof("actions.split run err=%v \n", err)
		return nil, err
	}

	glog.V(3).Infof("actions.split run results=%v\n", g.Results)

	// What to collect
	c := ctx.Pop("collect")

	// If `collect` is a string -- add it to list
	var collect []string // context variables to collect
	if collectone, ok := c.String(); ok {
		collect = append(collect, collectone)
	} else if collect, ok = c.StringList(); !ok {
		return action.Map{"results": g.Results}, nil
	}

	// Collect results
	results := make(action.Map)
	for _, result := range g.Results {
		for _, key := range collect {
			var list []interface{}
			if results[key] != nil {
				list, _ = results[key].([]interface{})
			}
			results[key] = append(list, result.Value(key))
		}
	}

	glog.V(3).Infof("actions.split collect results=%v\n", results)

	return results, nil
}

// Run - actions.run - runs action using `name` and `ctx` from context.
func Run(ctx action.Map) (action.Map, error) {
	a := mapToAction(ctx)
	return local.Run(a)
}

// mapToAction - Converts a map to action.
func mapToAction(m action.Map) *action.Action {
	if m == nil {
		return nil
	}

	var (
		name, _    = m.Get("name").String()
		context, _ = m.Get("ctx").Map()
		next, _    = m.Get("next").Map()
	)

	return &action.Action{
		Name: name,
		Ctx:  context,
		Next: mapToAction(next),
	}
}

func init() {
	core.Add("actions.run", core.Function{
		Description: "Runs action",
		Arguments: core.Variables{
			"name": core.String("Action name", ""),
			"ctx":  core.Map("Action context", nil),
			"next": core.Interface("Next action (map or string)", nil),
		},
		Returns: core.Variables{
			"*": core.Interface("Action results", nil),
		},
		Function: Run,
	})
	core.Add("actions.split", core.Function{
		Description: "Iterates through lists, launches actions and collects results",
		Arguments: core.Variables{
			"collect": core.Interface("What to collect (if empty everything is collected)", nil),
			"split":   core.Map("What and how to split", nil),
		},
		Returns: core.Variables{
			"*": core.Interface("Collected results", nil),
		},
		Function: Split,
	})
}
