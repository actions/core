package regexpcore

import "regexp"
import "github.com/crackcomm/go-actions/action"

// Regexp - Regexp extractor.
type Regexp struct {
	Name    string
	Pattern string
	All     bool
}

// RegexpMap - Map of Regexp structures.
type RegexpMap map[string]*Regexp

// Compile - Compiles a regular expression.
func (r *Regexp) Compile() (*regexp.Regexp, error) {
	return regexp.Compile(r.Pattern)
}

// Extract - Extracts pattern from value under Name key in ctx.
func (r *Regexp) Extract(ctx action.Map) interface{} {
	// Get value `r.Name` from context
	value := ctx.Get(r.Name)

	// Pass if empty value
	if value.IsNil() {
		return nil
	}

	// Compile regular expression
	re, err := r.Compile()
	if err != nil {
		return nil
	}

	// If it's a string - use in it
	if strvalue, ok := value.String(); ok {
		// Find all
		if r.All {
			return re.FindAllString(strvalue, -1)
		}

		// Find one
		return re.FindString(strvalue)
	}

	// If it's a byte array - use in it
	if bytes, ok := value.Bytes(); ok {
		// Find all
		if r.All {
			return re.FindAll(bytes, -1)
		}

		// Find one
		return re.Find(bytes)
	}

	// If it's nothing else - pass
	return nil
}

// Extract - Extracts all Regexp in map.
func (m RegexpMap) Extract(ctx action.Map) (result action.Map) {
	result = make(action.Map)
	for name, re := range m {
		// Extract regexp and add to result if any
		if value := re.Extract(ctx); value != nil {
			result[name] = value
		}
	}
	return
}

// mapToRegexp - Converts a map to Regexp.
func mapToRegexp(m action.Map) *Regexp {
	var (
		pattern, _ = m.Get("pattern").String()
		name, _    = m.Get("name").String()
		all, _     = m.Get("all").Bool()
	)
	return &Regexp{
		Pattern: pattern,
		Name:    name,
		All:     all,
	}
}

// mapToRegexpMap - Converts a map to RegexpMap.
func mapToRegexpMap(m action.Map) (remap RegexpMap) {
	remap = make(RegexpMap)
	for name := range m {
		// Get and convert value to a Map
		if m, ok := m.Get(name).Map(); ok {
			// Convert Map to Regexp
			re := mapToRegexp(m)

			// If Regexp name is empty use key value
			if re.Name == "" {
				re.Name = name
			}

			// Add Regexp to RegexpMap
			remap[name] = re
		}
	}
	return
}
