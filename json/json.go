package jsoncore

import "encoding/json"
import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/action"

// Decode - json.decode - decodes all JSON values in context from `decode` list.
// Example:
// 	{
//		"name": "json.decode",
//		"ctx": {
//			"decode": ["content"],
//			"content": "[1, 2, 3]"
//		}
//	}
func Decode(ctx action.Map) (action.Map, error) {
	// Pop decode value - context keys to decode
	ctx.Transform(ctx.Pop("decode"), decodeFunc)

	return ctx, nil
}

// Encode - json.encode - encodes all values in context from `encode` list to JSON.
// Example:
// 	{
//		"name": "json.encode",
//		"ctx": {
//			"encode": ["content"],
//			"content": [1, 2, 3]
//		}
//	}
func Encode(ctx action.Map) (action.Map, error) {
	// Pop encode value - context keys to encode
	ctx.Transform(ctx.Pop("encode"), encodeFunc)
	return ctx, nil
}

// decodeFunc - Decodes JSON strings, readers and byte arrays.
func decodeFunc(val action.Format) interface{} {
	// Value can be a Reader
	if reader, ok := val.Reader(); ok {
		defer reader.Close()
		var res interface{}
		if err := json.NewDecoder(reader).Decode(&res); err == nil {
			return res
		}
		return reader
	}

	return val.Value
}

// encodeFunc - Encodes value to a JSON byte array.
func encodeFunc(val action.Format) interface{} {
	// Try to format to a map and marshal (otherwise UnsupportedTypeError could appear on interface-interface map)
	if m, ok := val.Map(); ok {
		body, _ := json.Marshal(m)
		return body
	}
	// Otherwise marshal interface
	body, _ := json.Marshal(val.Value)
	return body
}

func init() {
	core.Add("json.decode", core.Function{
		Description: "Decode JSON values",
		Arguments: core.Variables{
			"decode": core.Interface("Name, a list or a map of values to decode", nil),
		},
		Returns: core.Variables{
			"*": core.Interface("Decoded values", nil),
		},
		Function: Decode,
	})

	core.Add("json.encode", core.Function{
		Description: "Encode JSON values",
		Arguments: core.Variables{
			"encode": core.Interface("Name, a list or a map of values to encode", nil),
		},
		Returns: core.Variables{
			"*": core.Interface("Encoded values", nil),
		},
		Function: Encode,
	})
}
