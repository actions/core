package contextcore

import "errors"
import "github.com/golang/glog"
import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/action"

// ErrPushUnexpected - Returned when `push` was neither string and a map.
var ErrPushUnexpected = errors.New("Push argument was unexpected value (must be string or map)")

// Push - Pushes values from `push` context map to context.
func Push(ctx action.Map) (action.Map, error) {
	p := ctx.Pop("push")

	// If it is a string push current context under `push` name to a new one
	if push, ok := p.String(); ok {
		clean := make(action.Map)
		clean.Push(push, ctx)
		return clean, nil
	}

	// If it is a map iterate through `push` map and pull values by `key` from context and push under `value`
	if push, ok := p.Map(); ok {
		for key := range push {
			if value := ctx.Pop(key); !value.IsNil() {
				name, _ := push.Get(key).String()
				ctx.Push(name, value.Value)
				glog.V(3).Infof("Pushing %#v value %#v under name %#v", key, value, name)
			}
		}
		return ctx, nil
	}

	return nil, ErrPushUnexpected
}

// Clear - Removes all context variables except those defined in `context` argument.
func Clear(ctx action.Map) (action.Map, error) {
	leave := ctx.Pull("context")

	// If `context` argument is empty -- clear entire context
	if leave.IsNil() {
		return action.Map{}, nil
	}

	// If `context` is a map -- it's a new context
	if clean, ok := leave.Map(); ok {
		return clean, nil
	}

	// If `context` is a string -- leave one variable
	if name, ok := leave.String(); ok {
		// Pop value
		value := ctx.Pop(name).Value

		// Close old context
		ctx.Close()
		return action.Map{name: value}, nil
	}

	// If `context` is a list -- create a new context and close old one
	list, _ := ctx.Pull("context").StringList()
	clean := make(action.Map)

	// Save all values from list
	for _, name := range list {
		value := ctx.Pop(name).Value
		clean.Add(name, value)
	}

	// Close old context
	ctx.Close()

	return clean, nil
}

func init() {
	var pushDocs = `Values to push (if a map -- keys are current name and values are names to push under,` +
		`if a string -- entire context is pushed under this name)`
	core.Add("context.push", core.Function{
		Description: "Pushes values to context",
		Arguments: core.Variables{
			"push": core.Interface(pushDocs, nil),
		},
		Returns: core.Variables{
			"*": core.Interface("Pushed values", nil),
		},
		Function: Push,
	})
	core.Add("context.clear", core.Function{
		Description: "Clear context and leave only values set in `context` argument",
		Arguments: core.Variables{
			"context": core.List("List of context variables NOT to clear", nil),
		},
		Returns: core.Variables{
			"*": core.Interface("Variables defined by `context` argument values", nil),
		},
		Function: Push,
	})
}
