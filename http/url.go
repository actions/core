package httpcore

import "net/url"
import "github.com/crackcomm/go-actions/action"

// joinURL - joins query to uri
func joinURL(uri string, query action.Format) (u *url.URL, err error) {
	u, err = url.Parse(uri)
	if err != nil {
		return
	}

	if query.IsNil() {
		return
	}

	var urlQuery url.Values
	if u.RawQuery == "" {
		urlQuery = make(url.Values)
	} else {
		urlQuery, err = url.ParseQuery(u.RawQuery)
		if err != nil {
			return
		}
	}

	// If request query is map
	if q, ok := query.Map(); ok {
		urlQuery = mapToQuery(urlQuery, q)
	} else if q, ok := query.String(); ok { // or a string
		urlQuery, err = strToQuery(urlQuery, q)
		if err != nil {
			return
		}
	}

	u.RawQuery = urlQuery.Encode()

	return
}

func mapToQuery(q url.Values, query action.Map) url.Values {
	for key, value := range query {
		if v, ok := value.([]interface{}); ok {
			for _, val := range v {
				strValue, _ := val.(string)
				q.Add(key, strValue)
			}
		} else {
			strValue, _ := value.(string)
			q.Add(key, strValue)
		}
	}

	return q
}

func strToQuery(q url.Values, rawQuery string) (url.Values, error) {
	// We have to parse rawQuery
	var err error
	var pq url.Values
	pq, err = url.ParseQuery(rawQuery)
	if err != nil {
		return nil, err
	}

	for k, v := range pq {
		if q[k] == nil {
			q[k] = v
		} else {
			q[k] = append(q[k], v...)
		}
	}

	return q, nil
}
