package httpcore

import "io"
import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"

func TestHTTPRequest(t *T) {
	Convey("http.request", t, func() {
		Convey("Given just a url", func() {
			res, err := Request(Map{"url": "http://www.google.com/"})
			So(err, ShouldEqual, nil)
			Convey("It should return status 200 OK", func() {
				status, ok := res.Get("status").Int()
				ShouldBeTrue(ok)
				So(status, ShouldEqual, 200)
			})
			Convey("It should return body reader", func() {
				reader, ok := res["body"].(io.ReadCloser)
				ShouldBeTrue(ok)
				Convey("Body reader should close", func() {
					err := reader.Close()
					ShouldBeNil(err)
				})
			})
		})
		Convey("Given scheme, url, hostname and pathname and lowercase method", func() {
			res, err := Request(Map{
				"pathname": "/pkg/net/http/",
				"hostname": "golang.org",
				"method":   "get",
				"scheme":   "http",
				"url":      "https://old.org",
			})
			So(err, ShouldEqual, nil)
			Convey("It should return status 200 OK", func() {
				status, ok := res.Pop("status").Int()
				ShouldBeTrue(ok)
				So(status, ShouldEqual, 200)
			})
			Convey("It should return body reader", func() {
				reader, ok := res["body"].(io.ReadCloser)
				ShouldBeTrue(ok)
				Convey("Body reader should close", func() {
					err := reader.Close()
					ShouldBeNil(err)
				})
			})
			Convey("It should join url", func() {
				url, _ := res.Pop("url").String()
				So(url, ShouldEqual, "http://golang.org/pkg/net/http/")
			})
		})
	})
}
