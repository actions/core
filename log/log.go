package logcore

import "github.com/golang/glog"
import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/action"

// Write - Writes body to a log - `log.write`.
func Write(ctx action.Map) (action.Map, error) {
	// Get string body from arguments
	if body, ok := ctx.Get("body").String(); ok {
		// Log to console
		if glog.V(2) {
			glog.Info(body)
		}
	}

	return ctx, nil
}

func init() {
	core.Add("log.write", core.Function{
		Description: "Writes a log",
		Arguments: core.Variables{
			"body": core.String("Content to write", ""),
		},
		Function: Write,
	})
}
