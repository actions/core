package selector

import . "testing"
import . "github.com/smartystreets/goconvey/convey"

func TestGetExtractors(t *T) {
	Convey("Given text name", t, func() {
		Convey("It should return text extractor", func() {
			extractor := GetExtractor("text")
			So(extractor, ShouldEqual, TextExtractor)
		})
	})
	Convey("Given number name", t, func() {
		Convey("It should return number extractor", func() {
			extractor := GetExtractor("number")
			So(extractor, ShouldEqual, NumberExtractor)
		})
	})
	Convey("Given attribute name", t, func() {
		Convey("It should return attribute extractor", func() {
			extractor := GetExtractor("href")
			attr, ok := extractor.(*AttributeExtractor)
			So(ok, ShouldEqual, true)
			So(attr.Attribute, ShouldEqual, "href")
		})
	})
}
