package selector

import "io"
import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"

var testHTML = []byte(`<html>
<head><title>HTML Test</title></head>
<body><a id="1" href="1">1000$</a> <a id="2" href="2">$1.234</a> <a id="3" href="3">1 000 000</a> <a href="4"></a></body>
</html>`)

type tr struct{}

func (t *tr) Read(_ []byte) (n int, err error) {
	n = 10
	err = io.EOF
	return
}

func TestSelectorsMap_Extract(t *T) {
	Convey("Given title selector", t, func() {
		Convey("It should extract title", func() {
			s := SelectorsMap{"title": &Selector{Path: "title"}}
			resi, err := s.ExtractBytes(testHTML)
			So(err, ShouldEqual, nil)
			res, ok := resi.(Map)
			So(ok, ShouldEqual, true)
			title, ok := res.Get("title").String()
			So(ok, ShouldEqual, true)
			So(title, ShouldEqual, "HTML Test")
		})
	})
	Convey("Given empty path", t, func() {
		Convey("It should extract from current node", func() {
			s := SelectorsMap{"title": &Selector{Extractor: &Selector{Path: "title"}}}
			resi, err := s.ExtractBytes(testHTML)
			So(err, ShouldEqual, nil)
			res, ok := resi.(Map)
			So(ok, ShouldEqual, true)
			title, ok := res.Get("title").String()
			So(ok, ShouldEqual, true)
			So(title, ShouldEqual, "HTML Test")
		})
	})
	Convey("Given wrong path", t, func() {
		Convey("It should return nil", func() {
			s := SelectorsMap{"title": &Selector{Path: "#nonono", Extractor: &Selector{Path: "title"}}}
			resi, err := s.ExtractBytes(testHTML)
			So(err, ShouldEqual, nil)
			So(resi, ShouldEqual, nil)
		})
	})
	Convey("Given wrong html", t, func() {
		Convey("It should return nil", func() {
			s := SelectorsMap{"title": &Selector{Path: "#nonono", Extractor: &Selector{Path: "title"}}}
			_, err := s.ExtractReader(nil)
			So(err, ShouldEqual, ErrNoBody)
		})
	})
	Convey("Given number extractor", t, func() {
		Convey("It should extract number", func() {
			s := SelectorsMap{
				"1k1":  &Selector{Path: "#1", Extractor: NumberExtractor},
				"1k2":  &Selector{Path: "#2", Extractor: NumberExtractor},
				"1kk3": &Selector{Path: "#3", Extractor: NumberExtractor},
				"1kk4": &Selector{Path: "#5", Extractor: NumberExtractor},
			}
			resi, err := s.ExtractBytes(testHTML)
			So(err, ShouldEqual, nil)
			res, ok := resi.(Map)
			So(ok, ShouldEqual, true)

			v1k, _ := res.Get("1k1").String()
			So(v1k, ShouldEqual, "1000")
			v12k, _ := res.Get("1k2").String()
			So(v12k, ShouldEqual, "1234")
			v1kk, _ := res.Get("1kk3").String()
			So(v1kk, ShouldEqual, "1000000")
			So(res["1kk4"], ShouldEqual, nil)
		})
	})
	Convey("Given attribute extractor", t, func() {
		Convey("It should extract all links", func() {
			s := SelectorsMap{"links": &Selector{Path: "a", Extractor: &AttributeExtractor{"href"}}}
			resi, err := s.ExtractBytes(testHTML)
			So(err, ShouldEqual, nil)
			res, ok := resi.(Map)
			So(ok, ShouldEqual, true)
			links, ok := res.Get("links").List()
			So(ok, ShouldEqual, true)
			So(len(links), ShouldEqual, 4)
			So(links[0], ShouldEqual, "1")
			So(links[1], ShouldEqual, "2")
			So(links[2], ShouldEqual, "3")
			So(links[3], ShouldEqual, "4")
		})
	})
	Convey("Given nested selectors map", t, func() {
		Convey("It should extract nested entity", func() {
			s := SelectorsMap{
				"1k1":  &Selector{Path: "#1", Extractor: NumberExtractor},
				"1k2":  &Selector{Path: "#2", Extractor: NumberExtractor},
				"1kk3": &Selector{Path: "#3", Extractor: NumberExtractor},
				"nil":  &Selector{Path: "#nil", Extractor: NumberExtractor},
			}
			s2 := SelectorsMap{"entity": s}
			resi, err := s2.ExtractBytes(testHTML)
			So(err, ShouldEqual, nil)
			res2, ok := resi.(Map)
			So(ok, ShouldEqual, true)
			res, ok := res2.Get("entity").Map()
			So(ok, ShouldEqual, true)

			v1k, _ := res.Get("1k1").String()
			So(v1k, ShouldEqual, "1000")
			v12k, _ := res.Get("1k2").String()
			So(v12k, ShouldEqual, "1234")
			v1kk, _ := res.Get("1kk3").String()
			So(v1kk, ShouldEqual, "1000000")
			So(res["nil"], ShouldEqual, nil)
		})
	})
}
