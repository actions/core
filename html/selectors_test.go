package htmlcore

import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/crackcomm/go-core/html/selector"
import . "github.com/smartystreets/goconvey/convey"

var testHTML = []byte(`<html><title>HTML Test</title></html>`)

func TestExtract(t *T) {
	Convey("Given selectors and body", t, func() {
		Convey("It should extract selectors from body", func() {
			s := Map{"title": "title"}
			ctx, err := Extract(Map{"body": testHTML, "selectors": s})
			So(err, ShouldEqual, nil)
			So(ctx["title"], ShouldEqual, "HTML Test")
		})
	})
	Convey("Given no body", t, func() {
		Convey("It should return error", func() {
			_, err := Extract(Map{})
			So(err, ShouldEqual, ErrNoBody)
		})
	})
	Convey("Given no selectors but body", t, func() {
		Convey("It should return error", func() {
			_, err := Extract(Map{"body": testHTML})
			So(err, ShouldEqual, ErrNoSelectors)
		})
	})
	Convey("Given empty selectors", t, func() {
		Convey("It should return error", func() {
			_, err := Extract(Map{"body": testHTML, "selectors": Map{}})
			So(err, ShouldEqual, ErrNoResults)
		})
	})
}

func TestExtractSelectors(t *T) {
	Convey("Given no body", t, func() {
		Convey("It should return error", func() {
			_, err := ExtractSelectors(Map{}, nil)
			So(err, ShouldEqual, ErrNoBody)
		})
	})
}

func TestIsSelector(t *T) {
	Convey("Given string selector", t, func() {
		Convey("It should pass test", func() {
			s := Map{"title": "title"}
			So(IsSelector(s.Get("title")), ShouldEqual, true)
		})
	})
	Convey("Given map selector", t, func() {
		Convey("It should pass test", func() {
			s := Map{
				"title": Map{
					"$path":    "title",
					"$extract": "text",
				},
			}
			So(IsSelector(s.Get("title")), ShouldEqual, true)
		})
	})
	Convey("Given map with extract only", t, func() {
		Convey("It should pass the test", func() {
			s := Map{"title": Map{"$extract": "text"}}
			So(IsSelector(s.Get("title")), ShouldEqual, true)
		})
	})
	Convey("Given empty map", t, func() {
		Convey("It should NOT pass the test", func() {
			s := Map{"title": Map{}}
			So(IsSelector(s.Get("title")), ShouldEqual, false)
			So(IsSelector(Format{Value: s}), ShouldEqual, false)
		})
	})
	Convey("Given list selector", t, func() {
		Convey("It should NOT pass the test", func() {
			s := Map{"title": []string{"title"}}
			So(IsSelector(s.Get("title")), ShouldEqual, false)
		})
	})
}

func TestToSelector(t *T) {
	Convey("Given unexpected selector", t, func() {
		Convey("It should return empty selector", func() {
			sel := ToSelector(Format{
				Value: []string{"blah"},
			})
			So(sel, ShouldEqual, nil)
		})
	})
	Convey("Given selector with empty extract", t, func() {
		Convey("It should return selector with text extractor", func() {
			s := ToSelector(Format{
				Value: Map{
					"$path": "title",
				},
			})

			// Title selector
			title, ok := s.(*Selector)
			So(ok, ShouldEqual, true)
			So(title.Path, ShouldEqual, "title")
		})
	})
}

func TestMapSelectors(t *T) {
	Convey("Given string selector", t, func() {
		Convey("It should convert to selectors map", func() {
			res := MapSelectors(Map{"title": "title"})

			// Title selector
			title, ok := res["title"].(*Selector)
			So(ok, ShouldEqual, true)
			So(title.Path, ShouldEqual, "title")
			So(title.Extractor, ShouldEqual, TextExtractor)
		})
	})
	Convey("Given map selector", t, func() {
		Convey("It should convert to selectors map", func() {
			res := MapSelectors(Map{
				"title": Map{
					"$path":    "title",
					"$extract": "text",
				},
			})

			// Title selector
			title, ok := res["title"].(*Selector)
			So(ok, ShouldEqual, true)
			So(title.Path, ShouldEqual, "title")
			So(title.Extractor, ShouldEqual, TextExtractor)
		})
		Convey("It should convert nested selectors", func() {
			s := Map{
				"entity": Map{
					"title": Map{
						"$path":    "title",
						"$extract": "text",
					},
				},
			}
			res := MapSelectors(s)
			So(res["entity"], ShouldNotEqual, nil)
			selmap, ok := res["entity"].(SelectorsMap)
			So(ok, ShouldEqual, true)
			sel, ok := selmap["title"].(*Selector)
			So(ok, ShouldEqual, true)
			So(sel.Path, ShouldEqual, "title")
			So(sel.Extractor, ShouldEqual, TextExtractor)
		})
	})
	Convey("Given unexpected extract", t, func() {
		Convey("It should return empty selector", func() {
			res := MapSelectors(Map{
				"title": Map{
					"$extract": []string{"not valid"},
					"$path":    "valid",
				},
			})

			// Title selector
			So(res["title"], ShouldEqual, nil)
		})
	})
	Convey("Given nested selector", t, func() {
		Convey("It should return nested selector", func() {
			s := MapSelectors(Map{
				"entity": Map{
					"$path": "title",
				},
			})

			// Entity selector
			entity, ok := s["entity"].(*Selector)
			So(ok, ShouldEqual, true)
			So(entity.Path, ShouldEqual, "title")
		})
		Convey("It should return nested selector", func() {
			result := MapSelectors(Map{
				"entity": Map{
					"$extract": Map{
						"title": Map{
							"$path": "title",
						},
					},
					"$path": "head",
				},
			})

			// Entity selector extracting selectors map
			entity, ok := result["entity"].(*Selector)
			So(ok, ShouldEqual, true)
			So(entity.Path, ShouldEqual, "head")

			// Nexted extractos map
			nest, ok := entity.Extractor.(SelectorsMap)
			So(ok, ShouldEqual, true)

			// Nested title extractor
			title, ok := nest["title"].(*Selector)
			So(ok, ShouldEqual, true)
			So(title.Path, ShouldEqual, "title")
			So(title.Extractor, ShouldEqual, TextExtractor)
		})
		Convey("It should return nested selector", func() {
			result := MapSelectors(Map{
				"entity": Map{
					"$path": "head",
					"$extract": Map{
						"$path": "title",
					},
				},
			})

			entity, ok := result["entity"].(*Selector)
			So(ok, ShouldEqual, true)

			// Entity should be Selector with $path head and $extract should be selector `title/text()`
			So(entity.Path, ShouldEqual, "head")

			title, ok := entity.Extractor.(*Selector)
			So(ok, ShouldEqual, true)
			So(title.Path, ShouldEqual, "title")
			So(title.Extractor, ShouldEqual, TextExtractor)
		})
	})
}
